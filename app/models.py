from django.db import models

# Create your models here.

# null = true acepta campos nulos
# pelicula tiene una llave foranea(Relacion de uno a muchos) , y sea crea una bidireccion de uno a muchos

class Categoria(models.Model):
    nombre=models.CharField(max_length=100)
    descripcion=models.TextField()

    def __str__(self): #Representacion en texto de un objeto
        return self.nombre

    class Meta():
        app_label: 'app'

class Pelicula(models.Model):
    titulo=models.CharField(max_length=200)
    year = models.IntegerField(null=True)
    sinopsis=models.TextField()
    actores=models.TextField(null=True)
    categoria=models.ForeignKey(
        Categoria,
        related_name='peliculas',
        on_delete=models.PROTECT
    )

    def __str__(self):
        return self.titulo
    
    class Meta:
        app_label: 'app'
    
     