from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from .models import Categoria, Pelicula
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
""" lista=["Avatar","Shrek","Star","Hello","Gao"]
lista2=[
    {"id":0,"nombre":"Holaa1","year":1998},
    {"id":1,"nombre":"Holaa2","year":1998},
    {"id":2,"nombre":"Holaa3","year":2000},


] """

def index(request):
    return render(request,'app/index.html')

@login_required 
def peliculas(request):
    
    """ if not request.user.is_authenticated:
        return render(request,'app/403.html') """

    # Obtiene las peliculas
    listap = Pelicula.objects.all().order_by('year')
    # Crea el contexto
    contexto = {
        'peliculas': listap
    }

    return render(request, 'app/peliculas.html', contexto)
    
@login_required 
def pelicula(request,id):
    """ obj_pelicula = get_object_or_404(Pelicula,pk=id) """
    """ print(id) """ #Probar en consola
    obj_pelicula = Pelicula.objects.get(id=id)
    contexto={
        'pelicula':obj_pelicula
    }

    """ contexto={}
    
    if id < len(lista):
        contexto['pelicula']=lista[id] """

    return render(request,'app/pelicula.html',contexto)

@login_required 
def categorias(request):
    lista = Categoria.objects.all()
    contexto = {
        'categorias':lista
    }


    return render(request,'app/categorias.html',contexto)

@login_required 
def acercade(request):
    return render(request,'app/acercade.html')

@login_required 
def form_crear_categoria(request):
    return render(request,'app/formCrearCategoria.html')

@login_required 
def post_crear_categoria(request):
    #Leyendo datos
    nombre =request.POST['nombre']

    cat = Categoria(nombre=nombre)

    cat.save()

    return redirect('app:categorias')

@login_required 
def form_crear_pelicula(request):
    lista_categorias = Categoria.objects.all().order_by('nombre')
    contexto = {
        'categorias':lista_categorias
    }
    return render(request,'app/formCrearPelicula.html',contexto)

@login_required 
def post_crear_pelicula(request):

    titulo = request.POST['titulo']
    year = request.POST['year']
    sinopsis = request.POST['sinopsis']
    actores = request.POST['actores']
    id_categoria = int(request.POST['categoria'])

    #Obtiene la categoria

    categoria = Categoria.objects.get(pk=id_categoria)

    nueva_pel = Pelicula()
    nueva_pel.titulo=titulo
    nueva_pel.year=year
    nueva_pel.sinopsis=sinopsis
    nueva_pel.actores=actores
    nueva_pel.categoria=categoria

    nueva_pel.save()
    return redirect('app:peliculas')
@login_required 
def post_eliminar_categoria(request):

    borrar = Categoria(nombre='Drama')
    borrar = Categoria.delete()

    return redirect('app:categorias')

def form_registro(request):
    return render(request,'app/registro.html')

def bienvenida(request):
    return render(request,'app/bienvenida.html')

def post_registro(request):
    nombres = request.POST['nombres']
    usuario = request.POST['usuario']
    email = request.POST['email']
    clave = request.POST['clave']
    
    u = User()
    u.first_name=nombres
    u.username=usuario
    u.email=email
    u.set_password(clave)
    u.save()



    """ print(nombres,usuario,email,clave) """
    return redirect('app:bienvenida')


def form_login(request):
    return render(request,'app/login.html')


def post_login(request):
    usuario = request.POST['usuario']
    clave = request.POST['clave']
    u = authenticate(username=usuario,password=clave)
    
    if u is None:
        print('claves incorrectas')
        return HttpResponse("Su usuario o clave es incorrecta")
    else:
        login(request,u)
        return redirect('app:index')

def post_logout(request):
    logout(request)
    return redirect('app:index')