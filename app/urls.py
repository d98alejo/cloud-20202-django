from django.urls import path 
from . import views 
 
app_name = 'app' 
urlpatterns = [
    path('', views.index, name='index'), 
    path('peliculas/',views.peliculas,name='peliculas'), #el primero es para ver como se ve la url ,el segundo es como la variable
    path('registro/',views.form_registro,name='form_registro'),
    path('registro_post/',views.post_registro,name='post_registro'),
    path('bienvenida/',views.bienvenida,name='bienvenida'),
    path('login/',views.form_login,name='form_login'),
    path('login_post/',views.post_login,name='post_login'),
    path('logout/',views.post_logout,name='post_logout'),
    path('categorias/',views.categorias,name='categorias'),
    path('categorias/crear',views.form_crear_categoria,name='form_crear_categoria'),
    path('peliculas/<int:id>/', views.pelicula,name='pelicula'),
     path('peliculas/crear',views.form_crear_pelicula,name='form_crear_pelicula'),
    path('acercade', views.acercade,name='acercade'),
    path('categorias/crear_post',views.post_crear_categoria,name='post_crear_categoria'),
    path('peliculas/crear_post',views.post_crear_pelicula,name='post_crear_pelicula'),
    
]